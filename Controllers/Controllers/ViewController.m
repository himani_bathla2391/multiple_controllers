//
//  ViewController.m
//  Controllers
//
//  Created by Click Labs133 on 11/6/15.
//
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITextField *textfield;
@property (strong, nonatomic) IBOutlet UIView *view3;

@end

@implementation ViewController
@synthesize textfield;
@synthesize view3;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
  self.textfield.delegate=self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField.text isEqual:@"1"]) {
        [self performSegueWithIdentifier:@"first" sender:nil];
    }
        // [textField resignFirstResponder];
    else if ([textField.text isEqualToString:@"2"]){
        [self performSegueWithIdentifier:@"second" sender:nil];
    }
    return YES;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
